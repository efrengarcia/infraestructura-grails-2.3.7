<html>
<head>
	<title><g:message code="springSecurity.login.title"/></title>
	<meta name="layout" content="kickstart" />
	<g:set var="layout_nomainmenu"		value="${true}" scope="request"/>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	<r:require modules="bootstrap"/>
</head>

<body>

<form class="form-signin" role="form" action='${postUrl}' method='POST' id="loginForm" name="loginForm" autocomplete='off'>
    <h2 class="form-signin-heading"><g:message code='spring.security.ui.login.signin'/></h2>
    <input name="j_username" class="form-control" id="username" placeholder="Usuario" required autofocus>
    <input name="j_password" type="password" class="form-control" id="password" placeholder="Password" required />
    <label class="checkbox">
        <input type="checkbox" value="remember-me"> <g:message code='spring.security.ui.login.rememberme'/>
    </label>
    <button class="btn btn-lg btn-primary btn-block" type="submit" elementId='loginButton' form='loginForm' messageCode='spring.security.ui.login.login'> <g:message code='spring.security.ui.login.login'/> </button>
</form>

</body>
</html>
