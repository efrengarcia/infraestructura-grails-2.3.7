package com.rs.gral

/**
 * RsGralPais
 * A domain class describes the data object and it's mapping to the database
 */
class RsGralPais {

    String  cvePais
    String  nombrePais
    String  aliasPais
    String 	nacionalidad

    /* Default (injected) attributes of GORM */
//	Long	id
//	Long	version

    /* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
 	static	hasMany		= [estados: RsGralEstado]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 

    static mapping = {
    }

    static constraints = {
        cvePais(size:2..20, unique: true,nullable: false, blank: false)
        nombrePais(size:3..50, unique: true,nullable: false, blank: false)
        aliasPais(nullable: true)
        nacionalidad(nullable: false, size: 1..50)
        estados()
    }

    public String toString() {
        return "${nombrePais}";
    }


    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
