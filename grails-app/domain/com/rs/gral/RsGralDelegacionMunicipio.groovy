package com.rs.gral

/**
 * RsGralDelegacionMunicipio
 * A domain class describes the data object and it's mapping to the database
 */
class RsGralDelegacionMunicipio {

    String 	claveDelegacionMunicipio
    String  nombreDelegacionMunicipio
    String  nombreCiudad

    SortedSet asentamiento
    static hasMany   = [ asentamiento : RsGralAsentamiento ]
    static belongsTo = [ estado : RsGralEstado ]

    /* Default (injected) attributes of GORM */
//	Long	id
//	Long	version

    /* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 

    static mapping = {
        sort "nombreDelegacionMunicipio"
    }

    static constraints = {
        claveDelegacionMunicipio(size: 1..3, unique: false, nullable: false, blank: false)
        nombreDelegacionMunicipio(size:3..50, unique: false,nullable: false, blank: false)
        nombreCiudad(size:3..50, unique: false, nullable: true, blank: true)
    }

    String toString() {
        "${nombreDelegacionMunicipio}"
    }

    int compareTo(obj) {
        nombreDelegacionMunicipio.compareTo(obj.nombreDelegacionMunicipio)
    }

    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
