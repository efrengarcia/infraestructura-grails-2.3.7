package com.rs.gral

/**
 * RsGralDomicilio
 * A domain class describes the data object and it's mapping to the database
 */
class RsGralDomicilio {

    String  calle
    String  numeroInterior
    String  numeroExterior
    String  tipoVivienda
    Boolean esFiscal = false
    String  entreCalles
    Integer aniosResidencia
    String  comentarios

    RsGralAsentamiento rsGralAsentamiento

    static belongsTo = [persona : RsPersona]

    static constraints = {
        calle(size:5..100, nullable: false, blank: false)
        numeroInterior(nullable: true)
        numeroExterior(nullable: true)
        tipoVivienda inList:["PROPIA","RENTADA","FAMILIARES","HIPOTECADA","OTRO"], nullable :true, blank :true
        rsGralAsentamiento(nullable: false)
        esFiscal()
        comentarios(nullable: true,size:0..300)
        entreCalles size:0..300, nullable:true
        aniosResidencia nullable: true
        persona(nullable:true)
    }

    String toString() {
        "${calle} ${numeroInterior} ${numeroExterior} - CP:${rsGralAsentamiento.codigoPostal}"
    }

    /* Default (injected) attributes of GORM */
//	Long	id
//	Long	version

    /* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 

    static mapping = {
    }

    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
